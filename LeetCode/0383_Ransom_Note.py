"""
Test Date: 19 November, 2022 (reload, redone)
Runtime: 57 ms (Beats 91.82%)
Memory : 14.4 MB (Beats 11.77%)
"""

class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
        if len(ransomNote) > len(magazine):
            return False
        else:
            ransomNote = list(ransomNote)
            magazine = list(magazine)

            for i in range(len(ransomNote)):
                if ransomNote[i] in magazine:
                    magazine.remove(ransomNote[i])
                else:
                    return False
            return True

# The following code yields very poor performance: Runtime beats 5.3%; Memory beats 19.89%
# class Solution:
#     def canConstruct(self, ransomNote: str, magazine: str) -> bool:
#         if len(ransomNote) > len(magazine):
#             return False
#         else:
#             magazineCheck = [False]*len(magazine)
#             for i in range(0, len(ransomNote)):
#                 for j in range(0, len(magazine)):
#                     if magazineCheck[j] == False and ransomNote[i] == magazine[j]:
#                         magazineCheck[j] = True
#                         break
#             if magazineCheck.count(True) == len(ransomNote):
#                 return True

# The following code still yields poor performance: Runtime beats 50.92%; Memory beats 5.80%
# class Solution:
#     def canConstruct(self, ransomNote: str, magazine: str) -> bool:
#         if len(ransomNote) > len(magazine):
#             return False
#         else:
#             ransom = list(ransomNote)
#             maga = list(magazine)

#             for i in range(len(ransom)):
#                 if ransom[i] in maga:
#                     maga.remove(ransom[i])
#                 else:
#                     return False
#             return True

"""
Task Description:

Given two strings ransomNote and magazine, return true if ransomNote can be constructed by using the letters from magazine and false otherwise.

Each letter in magazine can only be used once in ransomNote.

 

Example 1:

Input: ransomNote = "a", magazine = "b"
Output: false
Example 2:

Input: ransomNote = "aa", magazine = "ab"
Output: false
Example 3:

Input: ransomNote = "aa", magazine = "aab"
Output: true
 

Constraints:

1 <= ransomNote.length, magazine.length <= 105
ransomNote and magazine consist of lowercase English letters.
Accepted
636.6K
Submissions
1.1M
Acceptance Rate
57.6%

Credits: LeetCode (https://leetcode.com)
Copyright ©️ 2022 LeetCode All rights reserved
"""
