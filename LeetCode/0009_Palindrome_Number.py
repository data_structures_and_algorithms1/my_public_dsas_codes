"""
Test Date: 21 March, 2024
Runtime: 49 ms (Beats 26.82%)
Memory : 11.63 MB (Beats 38.37%)
"""

class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        return str(x) == str(x)[::-1]

# My first trial:
#
# Test Date: 21 March, 2024
# Runtime: 51 ms (Beats 22.34%)
# Memory : 11.64 MB (Beats 38.37%)
#
# class Solution(object):
#     def isPalindrome(self, x):
#         """
#         :type x: int
#         :rtype: bool
#         """
#         temp = str(x)
#         temp2 = ""
#         for n in range(len(temp)):
#             temp2 += temp[len(temp)-(n+1)]
#         if temp == temp2:
#             # print(temp, temp2)
#             return True
#         else:
#             # print(temp, temp2)
#             return False

"""
Task Description:

Given an integer x, return true if x is a palindrome, and false otherwise.

Example 1:

Input: x = 121
Output: true
Explanation: 121 reads as 121 from left to right and from right to left.
Example 2:

Input: x = -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
Example 3:

Input: x = 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
 

Constraints:

-231 <= x <= 231 - 1
 

Follow up: Could you solve it without converting the integer to a string?

Credits: LeetCode (https://leetcode.com)
Copyright ©️ 2024 LeetCode All rights reserved
"""
